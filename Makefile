doc_sources = $(wildcard doc/*/*.md) $(wildcard doc/*.md)
outdoc_files = $(addprefix out/,$(doc_sources:.md=.html))

docassets = $(addprefix out/,$(wildcard doc/assets/*))

VERSION = $(shell node -e "console.log( require('./src/package.json').version )")
UNAME := $(shell uname -s)
IMAGE = opsperator/etherpad
PORT = 8080
FRONTNAME = opsperator
-include Makefile.cust

ensure_marked_is_installed:
	set -eu; \
	hash npm; \
	if [ $(shell npm list --prefix bin/doc >/dev/null 2>/dev/null; echo $$?) -ne "0" ]; then \
		npm ci --prefix=bin/doc; \
	fi

docs: ensure_marked_is_installed $(outdoc_files) $(docassets)

out/doc/assets/%: doc/assets/%
	mkdir -p $(@D)
	cp $< $@

out/doc/%.html: doc/%.md
	mkdir -p $(@D)
	node bin/doc/generate.js --format=html --template=doc/template.html $< > $@
ifeq ($(UNAME),Darwin)
	sed -i '' 's/__VERSION__/${VERSION}/' $@
else
	sed -i 's/__VERSION__/${VERSION}/' $@
endif

.PHONY: install
install:
	@@./bin/installDeps.sh

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task-build task-scan pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in postgres-secret postgres-deployment \
	    postgres-service configmap secret deployment service; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "ETHERPAD_REPOSITORY_REF=$$BRANCH" \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		| oc apply -f-
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "ETHERPAD_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml | oc delete -f- || true

.PHONY: prep-k8s
prep-k8s:
	@@if test -s /opt/shared-init/settings.json; then \
	    rm -f settings.json; \
	    ln -sf /opt/shared-init/settings.json settings.json; \
	fi

.PHONY: prep-runtime
prep-runtime: prep-sso prep-k8s
	@@if ! test -s ./src/staic/js/jquery.js; then \
	    if test -s ./jquery.js; then \
		cp ./jquery.js ./src/static/js/; \
	    fi; \
	fi
	@@if test -d ./patch; then \
	    find patch -type f | while read p; \
	    do \
		d=`echo "$$p" | sed 's|^patch/|src/|'`; \
		echo "NOTICE: patching $$p"; \
		cat "$$p" >"$$d"; \
	    done; \
	fi

.PHONY: prep-sso
prep-sso:
	@@if test -s ./lemonldap-ng.ini.tpl; then \
	    test -z "$$ETHERPAD_API_HOSTNAME" && ETHERPAD_API_HOSTNAME=localhost; \
	    ETHERPAD_SHORT_DOMAINNAME=`echo "$$ETHERPAD_API_HOSTNAME" | cut -d. -f1`; \
	    ETHERPAD_REAL_DOMAINNAME=`echo "$$ETHERPAD_API_HOSTNAME" | sed 's|^[^\.]*\.||'`; \
	    test -z "$$ETHERPAD_REAL_DOMAINNAME" && ETHERPAD_REAL_DOMAINNAME=demo.local; \
	    test -z "$$LDAP_BASE" && LDAP_BASE=dc=demo,dc=local; \
	    test -z "$$ETHERPAD_LLNG_BIND_DN" && ETHERPAD_LLNG_BIND_DN="cn=ssoapp,ou=services,$$LDAP_BASE"; \
	    test -z "$$ETHERPAD_LLNG_BIND_PW" && ETHERPAD_LLNG_BIND_PW=secret; \
	    test -z "$$ETHERPAD_LLNG_CONF_DN" && ETHERPAD_LLNG_CONF_DN="ou=lemonldap,ou=config,$$LDAP_BASE"; \
	    test -z "$$LDAP_URL" && LDAP_URL=ldap://localhost; \
	    test -s /certs/ca.crt && LDAP_CA_FILE="ldapCAFile = /certs/ca.crt" || LDAP_CA_FILE=; \
	    sed -e "s|SED_API_HOSTNAME|$$ETHERPAD_SHORT_DOMAINNAME|g" \
		-e "s|SED_API_DOMAINNAME|$$ETHERPAD_REAL_DOMAINNAME|g" \
		-e "s|SED_LDAP_CA_FILE|$$LDAP_CA_FILE|" \
		-e "s|SED_LLNG_BIND_DN|$$ETHERPAD_LLNG_BIND_DN|" \
		-e "s|SED_LLNG_BIND_PW|$$ETHERPAD_LLNG_BIND_PW|" \
		-e "s|SED_LLNG_CONF_DN|$$ETHERPAD_LLNG_CONF_DN|" \
		-e "s|SED_LLNG_BACKEND|$$LDAP_URL|" ./lemonldap-ng.ini.tpl >lemonldap-ng.ini; \
	    echo "done initializing link to LLNG"; \
	fi

.PHONY: run
run:	start

.PHONY: demodb
demodb:
	@@docker rm -f testpostgres || true
	@@docker run --name testpostgres \
	   -e POSTGRESQL_USER=pguser \
	   -e POSTGRESQL_DATABASE=pgdb \
	   -e POSTGRESQL_PASSWORD=pgpassword \
	   -p 5432:5432 \
	   -d docker.io/centos/postgresql-12-centos7:latest

.PHONY: demo
demo: demodb
	@@docker rm -f testetherpad || true
	@@mkdir -p wip
	@@cat deploy/kubernetes/configmap.yaml | grep '^    ' | sed 's|^    ||' \
	    >wip/settings.json;
	@@pgip=`docker inspect testpostgres | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	echo "Working with postgres=$$pgip"; \
	sed -i \
	    -e "s|DB_HOST_PLACEHOLDER|$$pgip|" \
	    -e "s|DB_NAME_PLACEHOLDER|pgdb|" \
	    -e "s|DB_PASS_PLACEHOLDER|pgpassword|" \
	    -e "s|DB_PORT_PLACEHOLDER|5432|" \
	    -e "s|DB_USER_PLACEHOLDER|pguser|" \
	    wip/settings.json
	@@sleep 5
	@@docker run --name testetherpad \
	  -v `pwd`/wip:/opt/shared-init \
	  -e DEBUG=yay \
	  -e CI_START=yay \
	  -e ETHERPAD_API_HOSTNAME=pad.demo.local \
	  -e NODE_ENV=production \
	  -d $(IMAGE)

.PHONY: start
start: prep-runtime
	@@set -x;
	if test -z "$$PORT"; then \
	    export PORT=$(PORT); \
	fi; \
	if test "$$DO_PM2"; then \
	    if test -s /var/run/secrets/kubernetes.io/serviceaccount/token; then \
		pm2 start ecosystem.config.js --env k8s --no-daemon --no-vizion; \
	    else \
		pm2 start ecosystem.config.js --no-vizion; \
	    fi; \
	else \
	    node src/node/server.js; \
	fi

.PHONY: status
status:
	@@pm2 list

.PHONY: stop
stop:
	@@pm2 stop ecosystem.config.js

.PHONY: test
test:
	@@echo FIXME

.PHONY: clean
clean:
	@@rm -rf out/

module.exports = {
	apps : [
		{
		    name: 'front',
		    script: 'src/node/server.js',
		    instances: 1,
		    env_k8s: {
			    'NODE_ENV': 'production',
			    'PORT': '8080'
			},
		    env_production: {
			    'NODE_ENV': 'production',
			    'PORT': '8080'
			}
		}
	    ]
    };

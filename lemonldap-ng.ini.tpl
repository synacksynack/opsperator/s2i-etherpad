[all]
[configuration]
type                 = LDAP
ldapServer           = SED_LLNG_BACKEND
ldapConfBase         = SED_LLNG_CONF_DN
ldapBindDN           = SED_LLNG_BIND_DN
ldapBindPassword     = SED_LLNG_BIND_PW
ldapObjectClass      = applicationProcess
ldapAttributeId      = cn
ldapAttributeContent = description
SED_LDAP_CA_FILE
[portal]
[handler]
status              = 0
useRedirectOnError  = 1
[manager]
[node-handler]
nodeVhosts          = SED_API_HOSTNAME.SED_API_DOMAINNAME, SED_API_HOSTNAME-green.SED_API_DOMAINNAME, SED_API_HOSTNAME-blue.SED_API_DOMAINNAME

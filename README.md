# k8sPad

Forked from github.com/ether/etherpad-lite

Depends on LLNG authenticating users.

## Fair Warning

LLNG integration status: WIP

## Quick Start

To contribute, or just test this code on your local station, you may start
services with the following:

```
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
$ nvm install 8.11.3
$ nvm use 8.11.3
$ npm install -g pm2 node-gyp jsdoc
$ npm install
$ export LDAP_URL=ldap://toto.demo.local LDAP_BASE=dc=demo,dc=local LDAP_BINDDN=cn=admin,dc=demo,dc=local LDAP_BINDPW=secret
$ pm2 start ecosystem.config.js
```
